﻿using System;

namespace BasicExercises
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            //Basic Exercise 1
            Console.WriteLine("1.Write a C# Sharp program to print Hello and your name in a separate line." 
                              +"\nHello" 
                              +"\nTommy");
            //Basic Exercise 2
            Console.WriteLine("2.Write a C# Sharp program to print the sum of two numbers\n" 
                              + (5 + 5));
            //Basic Exercise 3
            Console.WriteLine("3.Write a C# Sharp program to print the result of dividing two numbers\n" 
                              + (4 / 2));
            //Basic Exercise 4
            Console.WriteLine("4. Write a C# Sharp program to print the result of the specified operations" 
                              +"\n・-1+4*6 = "+ (-1 + 4 * 6) 
                              +"\n・(35+5)%7 = "+ ((35+5)%7)
                              +"\n・14+-4*6/11 = "+ (14+-4*6/11)
                              +"\n・2+15/6*1-7%2 = "+ (2 + 15 / 6 * 1 - 7 % 2));
        }
    }
}
